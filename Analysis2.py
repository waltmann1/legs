from __future__ import division
import numpy as np
import gsd.hoomd
import gsd.fl
import numpy as np
import numpy.linalg as la
import copy as cp
from matplotlib import pyplot as plt
import hoomd.data as hbox
from mpl_toolkits.mplot3d import Axes3D

class Analysis2(object):

    def __init__(self, gsd_name, map_name, cut=.6):
        f = gsd.fl.open(name=gsd_name, mode='rb', application='', schema='hoomd',
                        schema_version=[1, 0])
        self.trajectory = gsd.hoomd.HOOMDTrajectory(f)
        self.invaders = None
        self.binders = None
        self.all_binders = None
        self.all_invaders = None
        self.arms = None
        self.xs = None
        self.ps = None
        self.temps = None
        self.read_map(map_name)
        self.frames = []
        self.topology = []
        self.connections = []
        self.bound = []
        self.box = self.trajectory.read_frame(0).configuration.box[:3]
        self.box_o = hbox.boxdim(Lx=self.box[0], Ly=self.box[1], Lz=self.box[2])
        self.cut =cut

    def read_map(self, map_name):

        f = open(map_name)
        data = f.readlines()
        self.binders = []
        self.all_binders = []
        self.invaders = []
        self.all_invaders = []
        self.arms = []
        self.xs = []
        self.ps = []
        self.temps = []
        on = 1000
        symbols = ['i', 'b', 'p', 'x', 'a', 't']
        mer_binders = []
        mer_invaders = []
        mer_arms = []
        mer_xs = []
        mer_ps = []
        mer_temps = []
        for line in data:
            s = line.split()
            for bit in s:
                if bit.isalpha():
                    on = symbols.index(bit)
                elif symbols[on] == 'i':
                    mer_invaders.append(int(bit))
                    self.all_invaders.append(int(bit))
                elif symbols[on] == 'b':
                    mer_binders.append(int(bit))
                    self.all_binders.append(int(bit))
                elif symbols[on] == 'p':
                    mer_ps.append(int(bit))
                elif symbols[on] == 'x':
                    mer_xs.append(int(bit))
                elif symbols[on] == 'a':
                    mer_arms.append(int(bit))
                elif symbols[on] == 't':
                    mer_temps.append(int(bit))
            if symbols[on] != 't':
                self.invaders.append(mer_invaders)
                mer_invaders = []
                self.binders.append(mer_binders)
                mer_binders = []
                self.ps.append(mer_ps)
                mer_ps = []
                self.xs.append(mer_xs)
                mer_xs = []
                self.arms.append(mer_arms)
                mer_arms = []
            else:
                self.temps.append(mer_temps)
                mer_temps = []

    def mer_topology(self, frame):

        frame = self.trajectory.read_frame(frame)
        top = []
        cd_connections = []
        for ind, mer in enumerate(self.invaders):
            connections = []
            explicit_connections = []
            for c in mer:
                c_pos = np.add(frame.particles.position[c], np.multiply(self.box, frame.particles.image[c]))
                found = False
                for mer_ind, sites in enumerate(self.binders):
                    if not found:
                        for d in sites:
                            if la.norm(np.subtract(c_pos, np.add(frame.particles.position[d], np.multiply(self.box, frame.particles.image[d])))) < self.cut:
                                found = True
                                connections.append(mer_ind)
                                explicit_connections.append([c,d])

            top.append(connections)
            cd_connections.append(explicit_connections)

        return top, cd_connections

    def chain_positions(self, frame_number):

        last = self.invaders[-1][-1]
        chain_pos = []
        frame = self.trajectory.read_frame(frame_number)

        for ind, part in enumerate(frame.particles.position):
            realpos = self.box_o.min_image(tuple(np.add(part, np.multiply(self.box, frame.particles.image[ind]))))
            if ind > last:
                chain_pos.append(realpos)

        return chain_pos

    def bound_mers(self, frame_number):
        frame = self.trajectory.read_frame(frame_number)

        bound = []
        for i in range(len(self.invaders)):
            b = -1
            position = self.box_o.min_image(tuple(np.add(frame.particles.position[i],
                                                         np.multiply(self.box, frame.particles.image[i]))))
            for mer_ind, array in enumerate(self.temps):
                for part_ind in array:
                    pos = self.box_o.min_image(tuple(np.add(frame.particles.position[part_ind],
                                 np.multiply(self.box, frame.particles.image[part_ind]))))
                    if la.norm(np.subtract(pos, position)) < 5:
                        b = mer_ind
            bound.append(b)
        #quit()
        return bound

    def read_frame(self, frame):

        if frame in self.frames:
            return
        else:
            self.frames.append(frame)
            top, connect = self.mer_topology(frame)
            self.topology.append(top)
            self.connections.append(connect)
            self.bound.append(self.bound_mers(frame))

    def average_connections(self, frame, bound=True):

        if frame not in self.frames:
            self.read_frame(frame)
            frame_idx = len(self.frames) - 1
        else:
            frame_idx = self.frames.index(frame)

        top = self.topology[frame_idx]
        b = self.bound[frame_idx]

        if bound:
            return np.average([len(cons) for ind,cons in enumerate(top) if len(cons) > 0 and b[ind] > -1])
        else:
            return np.average([len(cons) for ind,cons in enumerate(top) if len(cons) > 0 and b[ind] == -1])

    def invading_mers(self, frame, bound=True):

        if frame not in self.frames:
            self.read_frame(frame)
            frame_idx = len(self.frames) - 1
        else:
            frame_idx = self.frames.index(frame)

        top = self.topology[frame_idx]
        b = self.bound[frame_idx]

        if bound:
            return np.sum([1 for ind, cons in enumerate(top) if len(cons) > 0 and b[ind] > -1])
        else:
            return np.sum([1 for ind, cons in enumerate(top) if len(cons) > 0 and b[ind] == -1])

    def bound_correct(self, value, bound):

        if not bound:
            return value == -1
        else:
            return value > -1

    def bound_6nn_list(self, frame):

        if frame not in self.frames:
            self.read_frame(frame)
            frame_idx = len(self.frames) - 1
        else:
            frame_idx = self.frames.index(frame)

        b = self.bound[frame_idx]

        snap = self.trajectory.read_frame(frame)

        on_zero = [ind for ind, bound in enumerate(b) if bound == 0]

        list = [[] for _ in range(len(on_zero))]
        dist_list = [[] for _ in range(len(on_zero))]

        for oz_ind, mer_ind in enumerate(on_zero):
            for i in range(0, len(on_zero)):
                if i != oz_ind:
                    mer_ind2 = on_zero[i]
                    self.add_to_list_sorted(list[oz_ind], dist_list[oz_ind], mer_ind, mer_ind2, snap)
        return list,dist_list, on_zero

    def coordination_list(self, frame):

        list,dist_list, on_zero = self.bound_6nn_list(frame)

        fos = []
        snap = self.trajectory.read_frame(frame)

        for ind, sub_list in enumerate(dist_list):

            ave = np.average(sub_list[:5])
            std = np.std(sub_list[:5])
            #print(sub_list, ind)
            if sub_list[5] < 13:
                fos.append(6)
            else:
                fos.append(5)

        positions = [self.min_image_position(mer_ind, snap) for mer_ind in on_zero]


        return positions, fos




    def min_image_position(self, mer_ind, snap):

        return self.box_o.min_image(tuple(np.add(snap.particles.position[mer_ind],
                                                            np.multiply(snap.particles.image[mer_ind], self.box))))

    def add_to_list_sorted(self, mslist,msdlist, mer_ind, mer_ind2, snap):

        pos_two = self.min_image_position(mer_ind2, snap)
        pos_one = self.min_image_position(mer_ind, snap)
        dist = la.norm(np.subtract(pos_one, pos_two))
        #print(len(msdlist), len(mslist))
        if len(mslist) == 0:
            mslist.append(mer_ind2)
            msdlist.append(dist)
            #print(len(msdlist), len(mslist))
            #print("")
            return
        else:
            for nn_rank, mer in enumerate(mslist):
                dist2 = msdlist[nn_rank]
                if dist < dist2:
                    mslist.insert(nn_rank, mer_ind2)
                    msdlist.insert(nn_rank, dist)
                    if len(mslist) == 7:
                        #print(mslist)
                        #print(msdlist)
                        mslist.remove(mslist[6])
                        msdlist.remove(msdlist[6])
                    #print(len(msdlist), len(mslist))
                    #print("")
                    return
            if len(mslist) < 6:
                mslist.append(mer_ind2)
                msdlist.append(dist)
                #print(len(msdlist), len(mslist))
                #print("")
                return


    def hybridization_lifetimes(self,frames, bound=True):
        """

        :param frames: assumed to be in order first to last
        :param bound: for the mers bound to the template or not bound
        :return:
        """

        hybrids = []

        for frame in frames:
            if frame not in self.frames:
                self.read_frame(frame)
                frame_idx = len(self.frames) - 1
            else:
                frame_idx = self.frames.index(frame)

            con = self.connections[frame_idx]
            hybrids.append(con)

        active_connections = []
        all_connections = []
        length = []

        for mer_ind, mer in enumerate(hybrids[0]):
            for connect in mer:
                    frame_idx = self.frames.index(frames[0])
                    bound_array = self.bound[frame_idx]
                    if self.bound_correct(bound_array[mer_ind], bound):
                        all_connections.append(connect)
                        active_connections.append(connect)
                        length.append(1)

        for ind, f in enumerate(hybrids[1:]):
            now = [n_connect for mert in f for n_connect in mert]

            for con in active_connections:
                if con not in now:
                    active_connections.remove(con)

            for mer_ind, x in enumerate(f):
                frame_idx = self.frames.index(frames[ind + 1])
                bound_array = self.bound[frame_idx]
                if self.bound_correct(bound_array[mer_ind], bound):
                    for connect in x:
                        if connect in active_connections:
                            length[all_connections.index(connect)] += 1
                        else:
                            all_connections.append(connect)
                            length.append(1)
                            active_connections.append(connect)

        print(length)
        return all_connections, length

    def average_hybridization_lifetime(self, frames, bound=True):

        all_connections, length = self.hybridization_lifetimes(frames, bound=bound)

        return(np.average(length))

    def bound_radial_distribution(self, frame, cut_off=1000):

        if frame not in self.frames:
            self.read_frame(frame)
            frame_idx = len(self.frames) - 1
        else:
            frame_idx = self.frames.index(frame)

        bound = self.bound[frame_idx]

        snap = self.trajectory.read_frame(frame)

        mer_pos = [np.add(snap.particles.position[i], np.multiply(self.box, snap.particles.image[i])) for i in range(len(self.binders))]
        mer_pos = [self.box_o.min_image(tuple(that)) for that in mer_pos]
        dists = []

        for ind, pos in enumerate(mer_pos):
            for mer_ind in range(ind + 1, len(mer_pos)):
                dist = la.norm(np.subtract(pos, mer_pos[mer_ind]))
                if bound[ind] == bound[mer_ind] and bound[ind] != -1 and dist < max(self.box)/2:
                    dists.append(dist)

        return dists

    def graph_rdf(self, frame, cut_off=1000):

        dists = self.bound_radial_distribution(frame, cut_off=cut_off)
        rdf_hist, rbe = np.histogram(dists, bins=100)
        bin_middles = [(rbe[i] + rbe[i + 1]) / 2 for i in range(len(rbe) - 1)]

        pdf_hist = [rdf_hist[i] / (4 * np.pi * bin_middles[i] * bin_middles[i]) for i in range(len(rdf_hist))]

        total = np.sum(pdf_hist)

        pdf_hist = [p / total for p in pdf_hist]
        fig = plt.figure()

        ax1 = fig.add_subplot(111)

        ax1.set_title("Frame " + str(frame))

        ax1.set_xlabel('r')
        ax1.set_ylabel('')
        ax1.plot(bin_middles, rdf_hist, label='RDF')
        plt.show()

    def graph_rdf_overlay(self, frames, cut_off=1000):

        fig = plt.figure()

        ax1 = fig.add_subplot(111)

        #ax1.set_title("Frame " + str(frames))

        ax1.set_xlabel('r')
        ax1.set_ylabel('')
        for frame in frames:
            dists = self.bound_radial_distribution(frame, cut_off=cut_off)
            rdf_hist, rbe = np.histogram(dists, bins=100)
            bin_middles = [(rbe[i] + rbe[i + 1]) / 2 for i in range(len(rbe) - 1)]

            #pdf_hist = [rdf_hist[i] / (4 * np.pi * bin_middles[i] * bin_middles[i]) for i in range(len(rdf_hist))]

            total = np.sum(rdf_hist)

            pdf_hist = [p / total for p in rdf_hist]

            ax1.plot(bin_middles, rdf_hist, label="Frame "+str(frame))
        plt.legend()
        plt.show()

    def graph_rdf_combo(self, frames, cut_off=1000):

        fig = plt.figure()

        ax1 = fig.add_subplot(111)

        ax1.set_title("Frame " + str(frames))

        ax1.set_xlabel('r')
        ax1.set_ylabel('')
        all_dists = []
        flat = []
        for frame in frames:
            all_dists.append(self.bound_radial_distribution(frame, cut_off=cut_off))
        for dists in all_dists:
                for dist in dists:
                        flat.append(dist)
        rdf_hist, rbe = np.histogram(flat, bins=100)
        bin_middles = [(rbe[i] + rbe[i + 1]) / 2 for i in range(len(rbe) - 1)]

        #pdf_hist = [rdf_hist[i] / (4 * np.pi * bin_middles[i] * bin_middles[i]) for i in range(len(rdf_hist))]

        total = np.sum(rdf_hist)

        pdf_hist = [p / total for p in rdf_hist]

        ax1.plot(bin_middles, rdf_hist, label="Frame "+str(frame))
        plt.legend()
        plt.show()

    def graph_rdf_combo_overlay(self, set_frames, cut_off=1000):

        fig = plt.figure()

        ax1 = fig.add_subplot(111)

        #ax1.set_title("Frame " + str(frames))

        ax1.set_xlabel('r')
        ax1.set_ylabel('')

        for frames in set_frames:
            all_dists = []
            flat = []
            for frame in frames:
                all_dists.append(self.bound_radial_distribution(frame, cut_off=cut_off))
            for dists in all_dists:
                for dist in dists:
                    flat.append(dist)

            rdf_hist, rbe = np.histogram(flat, bins=100)
            bin_middles = [(rbe[i] + rbe[i + 1]) / 2 for i in range(len(rbe) - 1)]

            #pdf_hist = [rdf_hist[i] / (4 * np.pi * bin_middles[i] * bin_middles[i]) for i in range(len(rdf_hist))]

            total = np.sum(rdf_hist)
            rdf_hist = [p / len(frames) for p in rdf_hist]

            ax1.plot(bin_middles, rdf_hist, label="Frame "+str(frames[0]))
        plt.legend()
        plt.show()


    def graph_coord_sphere(self, frame):

        fig = plt.figure()

        ax1 = fig.add_subplot(111, projection='3d')

        # ax1.set_title("Frame " + str(frames))

        ax1.set_xlabel('x')
        ax1.set_ylabel('y')
        ax1.set_zlabel('z')
        positions, fos = self.coordination_list(frame)
        xs = [pos[0] for pos in positions]
        ys = [pos[1] for pos in positions]
        zs = [pos[2] for pos in positions]
        colors = []

        for coord in fos:
            if coord == 6:
                colors.append('k')
            else:
                colors.append('red')

        ax1.scatter(xs, ys, zs, c=colors, s=1000)

        plt.legend()
        plt.show()

    def get_doubles(self, frame):

        if frame not in self.frames:
            self.read_frame(frame)
            frame_idx = len(self.frames) - 1
        else:
            frame_idx = self.frames.index(frame)

        connections = self.connections[frame_idx]

        doubles = 0

        for own_mer_ind, mer in enumerate(connections):
            for connect in mer:
                own_bind_ind = self.invaders[own_mer_ind].index(connect[0])
                opp_mer_ind, opp_invader = self.get_binder_invader_pair_index(connect[1])
                for back_connect in connections[opp_mer_ind]:
                    if opp_invader in back_connect:
                        own_binder = back_connect[1]
                        if own_binder == self.binders[own_mer_ind][own_bind_ind]:
                            doubles += 1
        return doubles

    def get_binder_invader_pair_index(self, binder_ind):

        for mer_ind, mer in enumerate(self.binders):
            for bind_ind, ind in enumerate(mer):
                if ind == binder_ind:
                    return mer_ind, self.invaders[mer_ind][bind_ind]
